package class09.topic3;

import java.util.Scanner;

/**
 * @author zl
 */
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (true) {
            String s = scanner.nextLine();
            try {
                if (Integer.parseInt(s) < 100) {
                    throw new Exception100();
                } else if (Integer.parseInt(s) > 1000) {
                    throw new Exception1000();
                } else {
                    System.out.println("输入的数符合要求");
                    break;
                }
            } catch (Exception100 | Exception1000 e) {
                System.out.println(e);
            }

        }
    }
}

class Exception100
        extends Exception {
    @Override
    public String toString() {
        return "输入的数应该大于100";
    }
}

class Exception1000
        extends Exception {
    @Override
    public String toString() {
        return "输入的数应该小于1000";
    }
}
