package class03.test1;

class B {
    int x = 100, y = 200;

    public void setX(int x) {
        x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getSum() {
        return x + y;
    }
}

/**
 * @author zl
 */
public class Test1 {
    public static void main(String[] args) {
        B b = new B();
        b.setX(-100);
        b.setY(-200);
        System.out.println("sum=" + b.getSum());
    }
}