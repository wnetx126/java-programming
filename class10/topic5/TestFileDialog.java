package class10.topic5;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;

/**
 * @author zl
 */
public class TestFileDialog {
    public static void main(String[] args) {
        new FileFrame();
    }
}

class FileFrame
        extends Frame
        implements ActionListener {

    TextArea ta;

    Button open, quit;

    FileDialog fd;

    FileFrame() {

        super("获取并显示文本文件");

        ta = new TextArea(10, 45);

        open = new Button("open");

        quit = new Button("quit");

        open.addActionListener(this);

        quit.addActionListener(this);

        setLayout(new FlowLayout());

        add(ta);

        add(open);

        add(quit);

        setSize(350, 280);

        show();

        setLocationRelativeTo(null);

//        setDefaultCloseOperation(EXIT_ON_CLOSE);

    }

    @Override
    public void actionPerformed(ActionEvent e) {

        if ("open".equals(e.getActionCommand())) {

            fd = new FileDialog(this, "打开文件", FileDialog.LOAD);
            //设置文件对话框的基础目录
            fd.setDirectory("D:\\lenovo_download");
            fd.show(); //弹出并显示文件对话框，程序暂停直至用户选定一文件
            BufferedReader reader=null;
            try {
                reader = new BufferedReader(new FileReader(new File(fd.getDirectory(), fd.getFile())));
                String str;
                while ((str = reader.readLine()) != null) {
                    ta.append(str + "\n");
                }
            } catch (IOException exception) {
                exception.printStackTrace();
            }finally {
                try {
                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException exception) {
                    exception.printStackTrace();
                }
            }
            if ("quit".equals(e.getActionCommand())) {
                dispose();
                System.exit(0);
            }

        }
    }
}