package class10.topic4;

import java.io.EOFException;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;

/**
 * @Description:
 * @Author zl
 * @Date 2021/5/26 10:23
 */

public class Main {
    static final double PI =3.1415926;
    public static void main(String[] args) throws Exception {
        double numbers;
        try (RandomAccessFile rw = new RandomAccessFile(new File("test.txt"), "rw")) {
            for (int i = 1; i <= 10; i++) {
                rw.writeDouble(i * PI);
            }
            rw.seek(8 * 4);
            ArrayList<Double> doubles = new ArrayList<>();
            double double1;
            try {
                while ((double1 = rw.readDouble()) != -1) {
                    doubles.add(double1);
                }
            } catch (EOFException e) {
                System.out.println("已经读取到文件末尾");
            }
            rw.seek(8 * 4);
            rw.writeDouble(666);
            for (double aDouble : doubles) {
                rw.writeDouble(aDouble);
            }
            rw.seek(0);
            try {
                while ((numbers = rw.readDouble()) != -1) {
                    System.out.println(numbers);
                }
            } catch (EOFException e) {
                System.out.println("已经读取到文件末尾");
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
