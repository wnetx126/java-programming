package class10.topic1;

import java.io.*;

/**
 * @author zl
 */
public class Main {
    public static void main(String[] args){
        int n;
        File file = new File("test.txt");
        byte[] msg = "HelloWorld".getBytes();
        byte[] index = new byte[1024];
        FileOutputStream fileOutputStream = null;
        FileInputStream fileInputStream = null;
        try {
            if (file.exists()) {
                fileOutputStream = new FileOutputStream(file, true);
            } else {
                fileOutputStream = new FileOutputStream(file);
            }
            fileOutputStream.write(msg);
            fileInputStream = new FileInputStream(file);
            while ((n = fileInputStream.read(index)) != -1) {
                String s = new String(index, 0, n);
                System.out.println(s);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
                try {
                    if (fileInputStream != null) {
                        fileInputStream.close();
                    }
                } catch (IOException exception) {
                    exception.printStackTrace();
                }

                try {
                    if (fileOutputStream != null) {
                        fileOutputStream.close();
                    }
                } catch (IOException exception) {
                    exception.printStackTrace();
                }
            }
    }
}
