package class05.line;

/**
 * @author zl
 */
public class Line {
    Point p1;
    Point p2;

    public Line(Point p1, Point p2) {
        this.p1 = p1;
        this.p2 = p2;
    }

    public float getLength() {
        return (float) Math.sqrt(Math.pow(p1.getX() - p2.getX(), 2) + Math.pow(p1.getY() - p2.getY(), 2));
    }

    public float getSlope() {
        return (p1.getX() - p2.getX()) / (p1.getY() - p2.getY());
    }
}
