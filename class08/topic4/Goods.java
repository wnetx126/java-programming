package class08.topic4;

/**
 * @author zl
 */
public class Goods {
    private String goodId;
    private String goodName;
    private double goodPrice;

    public Goods(String goodId, String goodName, double goodPrice) {
        this.goodId = goodId;
        this.goodName = goodName;
        this.goodPrice = goodPrice;
    }

    public String getGoodId() {
        return goodId;
    }

    public void setGoodId(String goodId) {
        this.goodId = goodId;
    }

    public String getGoodName() {
        return goodName;
    }

    public void setGoodName(String goodName) {
        this.goodName = goodName;
    }

    public double getGoodPrice() {
        return goodPrice;
    }

    public void setGoodPrice(double goodPrice) {
        this.goodPrice = goodPrice;
    }

    @Override
    public String toString() {
        return goodId + "," + goodName + "," + goodPrice;
    }
}
