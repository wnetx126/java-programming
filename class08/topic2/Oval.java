package class08.topic2;

/**
 * @author zl
 */
public class Oval
        extends Shape {
    private double longRadius;
    private double shortRadius;

    public Oval(double a, double b) {
        longRadius = a;
        shortRadius = b;
    }

    public Oval() {
        longRadius = 0;
        shortRadius = 0;
    }

    @Override
    public String toString() {
        return "Oval(a:" + longRadius + "," + "b:" + shortRadius + ")";
    }

    @Override
    double area() {
        return PI * longRadius * shortRadius;
    }

    @Override
    double perimeter() {
        return 2 * PI * shortRadius + 4 * (longRadius - shortRadius);
    }
}
