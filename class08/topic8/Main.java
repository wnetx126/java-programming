package class08.topic8;

import java.util.Arrays;

/**
 * @author zl
 */
public class Main {
    public static void main(String[] args) {
        int[][] arr = new int[][]{
                {32, 87, 90}, {-4, 6}, {77, 15, 100, -23}, {5, -4, 7}
        };
        System.out.println(Arrays.deepToString(arr));
        for (int i = 0; i < arr.length; i++) {
            if (Arrays.binarySearch(arr[i], -4) >= 0) {
                System.out.println("(" + i + "," + Arrays.binarySearch(arr[i], -4) + ")");
            }
        }
        for (int i = 0; i < arr.length; i++) {
            if (Arrays.binarySearch(arr[i], -4) >= 0) {
                System.out.println("(" + i + "," + Arrays.binarySearch(arr[i], -4) + ")");
                break;
            }
        }
        for (int[] value : arr) {
            Arrays.sort(value);
        }
        System.out.println(Arrays.deepToString(arr));
    }
}
