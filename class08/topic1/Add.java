package class08.topic1;

/**
 * @author zl
 */
public class Add
        implements ICompute {
    @Override
    public int computer(int n, int m) {
        return m+n;
    }
}
