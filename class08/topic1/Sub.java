package class08.topic1;

/**
 * @author zl
 */
public class Sub implements ICompute{
    @Override
    public int computer(int n, int m) {
        return n-m;
    }
}
