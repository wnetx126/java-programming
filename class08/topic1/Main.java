package class08.topic1;

import java.util.Scanner;

/**
 * @author zl
 */
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int a=scanner.nextInt();
        int b=scanner.nextInt();
        Add add = new Add();
        Sub sub = new Sub();
        System.out.println(add.computer(a,b));
        System.out.println(sub.computer(a,b));
    }
}