package class08.topic9;


/**
 * @author zl
 */
public class Main {
    public static void main(String[] args) {
        //str1与str2都放在常量池中，指向的是同样的值，str3也指向相同的常量池中
        String str1 = "A String";
        //如果是new 出来的str对象则不在常量池中
        String str2 = "A String";

        String str3 = str1;
        System.out.println(str1 == str2);
        System.out.println(str1 == str3);
        System.out.println(str1.length());
        //equals比较的是其中的值是否相等
        System.out.println(str1.equals(str2));
        System.out.println(str1.equals(str3));
        //compareTo依次比较的是二者的字典序
        System.out.println(str1.compareTo(str2));
        System.out.println(str1.compareTo(str3));
        //连接两个字符串
        System.out.println(str1.concat(str3));
        //返回元素的位置
        System.out.println(str1.indexOf('t'));
        System.out.println(str3.lastIndexOf('t'));
        System.out.println(str1.indexOf("t"));
        System.out.println(str1.substring(2, 4));
    }
}
