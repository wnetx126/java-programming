package class08.topic10;

/**
 * @author zl
 */
public class FilenameDemo {
    public static void main(String[] args) {

        Filename myHomePage = new Filename("/home/mem/index.html", '/', '.');

        System.out.println("Extension = " + myHomePage.extension());

        System.out.println("Filename = " + myHomePage.filename());

        System.out.println("Path = " + myHomePage.path());

    }
}

/**
 * @author zl
 */
class Filename {                                    // Filename.java

    private String fullPath;

    private char pathSeparator, extensionSeparator;

    public Filename(String str, char sep, char ext) {

        fullPath = str; //全路径名

        pathSeparator = sep; //路径符

        extensionSeparator = ext; //扩展名符

    }

//取得扩展名

    public String extension() {
        return fullPath.split("\\.")[1];
    }

//取得文件名

    public String filename() {
        return fullPath.split("\\.")[0].split("\\/")[3];
    }

//取得路径

    public String path() {
        return "/" + fullPath.split("\\/")[3];
    }

}
