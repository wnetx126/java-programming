package class08.topic3;

import java.util.Scanner;

/**
 * @author zl
 */
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Books[] books = new Books[]{
                new Books(scanner.next(), scanner.nextDouble()),
                new Books(scanner.next(), scanner.nextDouble()),
                new Books(scanner.next(), scanner.nextDouble())
        };
        for (Books book : books) {
            System.out.println("书名：" + book.getBookName() + ", 书号：" + book.getBookId() + ", 书价：" + book.getBookPrice());
        }
        System.out.println("图书总册数为："+Books.getBookCount());
    }
}
