package class08.topic5;

import java.util.Scanner;

/**
 * @author zl
 */
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double[] eqn;
        try {
            eqn = new double[]{scanner.nextDouble(), scanner.nextDouble(), scanner.nextDouble()};
        } catch (Exception e) {
            System.out.println("Wrong Format");
            return;
        }
        double[] roots = new double[2];
        int rootCount = solveQuadratic(eqn, roots);
        if (rootCount == 1) {
            System.out.println("The equation has one root: " + String.format("%.4f", roots[0]));
        } else {
            System.out.println("The equation has two roots: " + String.format("%.4f", roots[0]) + " and " + String.format("%.4f", roots[1]));
        }
    }

    public static int solveQuadratic(double[] eqn, double[] roots) {
        try {
            roots[0] = (-1 * eqn[1] + Math.sqrt(Math.pow(eqn[1], 2) - 4 * eqn[0] * eqn[2])) / (2 * eqn[0]);
            roots[1] = (-1 * eqn[1] - Math.sqrt(Math.pow(eqn[1], 2) - 4 * eqn[0] * eqn[2])) / (2 * eqn[0]);
        } catch (Exception e) {
            System.out.println("The equation has no roots");
        }
        return roots[0] == roots[1] ? 1 : 2;
    }
}
