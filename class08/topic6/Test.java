package class08.topic6;


import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

/**
 * @author zl
 */
public class Test {
    public static void main(String[] args) {
        stack s = new stack();

        s.put('a');

        s.put('b');

        s.put('c');

        queue q = new queue();

        q.put('a');

        q.put('b');

        q.put('c');

        System.out.println("\n栈:");

        System.out.println(s.get());

        System.out.println(s.get());

        System.out.println(s.get());

        System.out.println("\n队列:");

        System.out.println(q.get());

        System.out.println(q.get());

        System.out.println(q.get());
    }
}

interface Access {
    char get();

    void put(char c);

}

class stack
        implements Access {
    Stack<Character> characters = new Stack<>();

    @Override
    public char get() {
        return characters.pop();
    }

    @Override
    public void put(char c) {
        characters.push(c);
    }     //后进先出

}

class queue
        implements Access {
    Queue<Character> queue = new LinkedList<>();

    @Override
    public char get() {
        return queue.poll();
    }

    @Override
    public void put(char c) {
        queue.offer(c);
    }     //先进先出

}
