package class07.circle;

import java.util.Arrays;

/**
 * @author zl
 */
public class TestCircle {
    public static void main(String[] args) {
        Circle[] circles = {new Circle(2), new Circle(10), new Circle(8), new Circle(4), new Circle(12)};
        Arrays.sort(circles);
        System.out.println(Arrays.toString(circles));
        System.out.println(Arrays.binarySearch(circles, new Circle(9)) >= 0 ? "找到了radius为9的圆，位置为:" + Arrays.binarySearch(circles, new Circle(9)) : "没有找到radius为9的圆");
        System.out.println(Arrays.binarySearch(circles, new Circle(10)) >= 0 ? "找到了radius为10的圆，位置为:" + Arrays.binarySearch(circles, new Circle(10)) : "没有找到radius为10的圆");
        Circle[] circles1=Arrays.copyOf(circles,4);
        System.out.println(Arrays.toString(circles1));
    }
}
