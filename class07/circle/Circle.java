package class07.circle;

/**
 * @author zl
 */
public class Circle implements Comparable<Circle>{
    private int radius;
    public Circle(int radius) {
        this.radius = radius;
    }
    public int getRadius() {
        return radius;
    }

    @Override
    public int compareTo(Circle circle) {
        return Integer.compare(this.getRadius(),circle.getRadius());
    }
    @Override
    public String toString(){
        return radius+" ";
    }
}
