package class07;

import java.util.Arrays;

/**
 * @author zl
 */
public class Array {
    public static void main(String[] args) {
        int[] a = new int[20];
        Arrays.fill(a,0,8,2);
        Arrays.fill(a,8,a.length,22);
        System.out.println(Arrays.toString(a));
    }
}
