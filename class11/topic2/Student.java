package class11.topic2;

import java.util.Objects;

/**
 * @ClassName Student
 * @Description 学生类
 * @Author zl
 * @Date 2021/6/2 11:47
 **/
public class Student {
    private int number;
    private String name;
    private int age;

    public Student(int number, String name, int age) {
        this.number = number;
        this.name = name;
        this.age = age;
    }

    public int getNumber() {
        return number;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    @Override
    public String toString() {
        return "sid:"+number+", "+"name:"+name+", "+"age:"+age;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Student student)) {
            return false;
        }
        return getAge() == student.getAge() && getName().equals(student.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getAge());
    }
}
