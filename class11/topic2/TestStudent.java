package class11.topic2;

import java.util.ArrayList;
import java.util.HashSet;

/**
 * @ClassName TestStudent
 * @Description 测试学生类
 * @Author zl
 * @Date 2021/6/2 11:50
 **/
public class TestStudent {
    public static void main(String[] args) {
        ArrayList<Student> students = new ArrayList<>();
        students.add(new Student(1, "Tom", 20));
        students.add(new Student(2, "Jack", 21));
        students.add(new Student(3, "John", 22));
        students.add(new Student(4, "Tom",20));
        students.add(new Student(5, "John", 22));
        HashSet<Student> studentsHashSet = new HashSet<>(students);
        for (Student student:studentsHashSet){
            System.out.println(student);
        }
    }
}
