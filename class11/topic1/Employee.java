package class11.topic1;

import java.util.Objects;

/**
 * @ClassName Employee
 * @Description 员工类
 * @Author zl
 * @Date 2021/6/1 18:30
 **/
public class Employee {
    private int number;
    private String name;
    private String department;
    private int wage;

    public Employee(int number, String name, String department, int wage) {
        this.number = number;
        this.name = name;
        this.department = department;
        this.wage = wage;
    }

    public int getNumber() {
        return number;
    }

    public String getName() {
        return name;
    }

    public String getDepartment() {
        return department;
    }

    public int getWage() {
        return wage;
    }
    public void changePay(int wage){
        this.wage=wage;
    }
    public void changePay(float percentage){
        this.wage= (int)(this.wage*(1+percentage));
    }
}
