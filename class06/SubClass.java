package class06;

/**
 * @author zl
 */
class Parent {
    private static String pStaticField = "父类--静态变量";
    private String pField = "父类--实例变量";

    static {
        System.out.println(pStaticField);
        System.out.println("父类--静态初始化块");
    }

    {
        System.out.println(pField);
        System.out.println("父类--实例初始化块");
    }

    public Parent() {
        System.out.println("父类--构造方法");
    }
}

/**
 * @author zl
 */
public class SubClass
        extends Parent {
    private static String sStaticField = "子类--静态变量";
    private String sField = "子类--实例变量";

    static {
        System.out.println(sStaticField);
        System.out.println("子类--静态初始化块");
    }

    {
        System.out.println(sField);
        System.out.println("子类--实例初始化块");
    }

    public SubClass() {
        System.out.println("子类--构造方法");
    }

    public static void main(String[] args) {
        new Parent();
        System.out.println("-----------------");
        new SubClass();
        System.out.println("-----------------");
        new SubClass();
    }
}
