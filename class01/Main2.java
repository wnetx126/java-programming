package class01;

/**
 * @author zl
 */
public class Main2 {
    public static void main(String[] args) {
        /**
         * i表示初始汽水
         * j表示初始空瓶子
         * k表示喝掉的汽水瓶数
         */
        int i = 1000, j = 0, k = 0;
        while (i != 0) {
            i--;
            k++;
            j++;
            if (j == 3) {
                i++;
                j = 0;
            }
        }
        System.out.println("喝掉的汽水:" + k);
        System.out.println("剩余的空瓶子:" + j);
    }
}
