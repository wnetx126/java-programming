package class01;

/**
 * @author zl
 */
public class Main4 {
    public static void main(String[] args) {
        int i, j;
        for (i=1;i<=9;i++){
            for (j=1;j<=i;j++){
                System.out.printf("%d*%d=%2d;",i,j,i*j);
            }
            System.out.println();
        }
    }
}
