package class01;

import java.util.HashMap;
import java.util.Scanner;

/**
 * @author zl
 */
public class Main3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int w = scanner.nextInt();
        /**
         * 使用HashMap键值对来存储值
         * w=0表示周日，w=1表示周一,..., w=6表示周六
         */
        HashMap<Integer, String> map = new HashMap<>(7);
        map.put(0, "周日");
        map.put(1, "周一");
        map.put(2, "周二");
        map.put(3, "周三");
        map.put(4, "周四");
        map.put(5, "周五");
        map.put(6, "周六");
        System.out.println("今天:" + map.get(w));
        if (w == 0) {
            w = w + 7;
        }
        System.out.println("昨天:" + map.get((w - 1) % 7));
        System.out.println("明天:" + map.get((w + 1) % 7));
    }
}
