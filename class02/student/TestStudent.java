package class02.student;


import java.util.ArrayList;
import java.util.Comparator;
import java.util.Scanner;

/**
 * @author zl
 */
public class TestStudent {
    public static void main(String[] args) {
        float average=0;
        System.out.println("请录入学生学号、姓名、性别、年龄、成绩:");
        Scanner scanner = new Scanner(System.in);
        ArrayList<Student> students = new ArrayList<>();
        for (int i=0;i<5;i++){
           students.add(new Student(scanner.next(), scanner.next(), scanner.next(), scanner.nextInt(), scanner.nextInt()));
       }
        students.sort(Comparator.comparingInt(Student::getJava));
        for (Student student:students){
            System.out.println("学号:"+student.getNo()+"姓名:"+student.getName()+"性别:"+student.getSex()+"年龄:"+student.getAge()+"成绩:"+student.getJava());
            average=average+student.getJava();
        }
        System.out.println("成绩平均值:");
        System.out.println(average*1.0/students.size());
        System.out.println("最小值:");
        System.out.println(students.get(0).getJava());
        System.out.println("最大值:");
        System.out.println(students.get(students.size()-1).getJava());
    }
}
