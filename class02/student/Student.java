package class02.student;

/**
 * @author zl
 */
public class Student {
    private String sNo;
    private String sName;
    private String sSex;
    private int sAge;
    private int sJava;

    public Student(String sNo, String sName, String sSex, int sAge, int sJava) {
        this.sNo = sNo;
        this.sName = sName;
        this.sSex = sSex;
        this.sAge = sAge;
        this.sJava = sJava;
    }

    public String getNo() {
        return sNo;
    }

    public String getName() {
        return sName;
    }

    public String getSex() {
        return sSex;
    }

    public int getAge() {
        return sAge;
    }

    public int getJava() {
        return sJava;
    }

}
