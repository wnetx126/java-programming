package class02.time;

/**
 * @author zl
 */
public class Time {
    private int hour;
    private int minute;
    private int second;

    Time(int hour, int minute, int second) {
        this.hour = hour;
        this.minute = minute;
        this.second = second;
    }

    Time() {
    }

    public void showTime() {
        System.out.print(hour + ":" + minute + ":" + second);
    }

    public void setTime(int i, int i1, int i2) {
        hour = i;
        minute = i1;
        second = i2;
    }
}
