package class02.time;

/**
 * @author zl
 */
public class TestTime {
    public static void main(String[] args) {
        Time t1 = new Time(10, 34, 44);
        Time t2 = new Time();
        t1.showTime();
        System.out.println();
        t2.showTime();
        System.out.println();
        t2.setTime(12, 50, 59);
        t2.showTime();
        System.out.println();
    }
}
