package class02.rectangle;

import java.util.Scanner;

/**
 * @author zl
 */
public class TestRectangle {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Rectangle rectangle = new Rectangle(scanner.nextFloat(), scanner.nextFloat(), scanner.nextFloat(), scanner.nextFloat());
        System.out.println("矩形面积:"+rectangle.area());
        System.out.println("矩形周长:"+rectangle.perimeter());
    }
}
